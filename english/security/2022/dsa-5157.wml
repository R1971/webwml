<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Jeffrey Bencteux reported two vulnerabilities in cifs-utils, the Common
Internet File System utilities, which can result in escalation of
privileges (<a href="https://security-tracker.debian.org/tracker/CVE-2022-27239">CVE-2022-27239</a>) or an information leak (<a href="https://security-tracker.debian.org/tracker/CVE-2022-29869">CVE-2022-29869</a>).</p>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 2:6.8-2+deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 2:6.11-3.1+deb11u1.</p>

<p>We recommend that you upgrade your cifs-utils packages.</p>

<p>For the detailed security status of cifs-utils please refer to its
security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cifs-utils">https://security-tracker.debian.org/tracker/cifs-utils</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5157.data"
# $Id: $

<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Lahav Schlesinger discovered a vulnerability in the revocation plugin of
strongSwan, an IKE/IPsec suite.</p>

<p>The revocation plugin uses OCSP URIs and CRL distribution points (CDP) which
come from certificates provided by the remote endpoint. The plugin didn't check
for the certificate chain of trust before using those URIs, so an attacker
could provided a crafted certificate containing URIs pointing to servers under
their control, potentially leading to denial-of-service attacks.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 5.9.1-1+deb11u3.</p>

<p>We recommend that you upgrade your strongswan packages.</p>

<p>For the detailed security status of strongswan please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/strongswan">\
https://security-tracker.debian.org/tracker/strongswan</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5249.data"
# $Id: $

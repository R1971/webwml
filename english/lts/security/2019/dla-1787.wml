<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple researchers have discovered vulnerabilities in the way the
Intel processor designs have implemented speculative forwarding of data
filled into temporary microarchitectural structures (buffers). This
flaw could allow an attacker controlling an unprivileged process to
read sensitive information, including from the kernel and all other
processes running on the system or cross guest/host boundaries to read
host memory.</p>

<p>See <a href="https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html">https://www.kernel.org/doc/html/latest/admin-guide/hw-vuln/mds.html</a>
for more details.</p>

<p>To fully resolve these vulnerabilities it is also necessary to install
updated CPU microcode. An updated intel-microcode package (only
available in Debian non-free) will be provided via a separate DLA. The
updated CPU microcode may also be available as part of a system firmware
("BIOS") update.</p>

<p>In addition, this update includes a fix for a regression causing
deadlocks inside the loopback driver, which was introduced by the update
to 4.9.168 in the last security update.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.9.168-1+deb9u2~deb8u1.</p>

<p>We recommend that you upgrade your linux-4.9 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1787.data"
# $Id: $

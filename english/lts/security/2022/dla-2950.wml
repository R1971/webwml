<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was found that Scrapy, a framework for extracting data from websites,
could send HTTP Authorization as well as cookies to other domains in case
of redirections, possibly leaking user credentials.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.0.3-2+deb9u1.</p>

<p>We recommend that you upgrade your python-scrapy packages.</p>

<p>For the detailed security status of python-scrapy please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/python-scrapy">https://security-tracker.debian.org/tracker/python-scrapy</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2950.data"
# $Id: $

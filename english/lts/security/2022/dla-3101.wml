<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in libxslt, an XSLT processing
runtime library, that could result in denial of service or potentially
the execution of arbitrary code if malicious files are processed.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1.1.32-2.2~deb10u2.</p>

<p>We recommend that you upgrade your libxslt packages.</p>

<p>For the detailed security status of libxslt please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxslt">https://security-tracker.debian.org/tracker/libxslt</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3101.data"
# $Id: $

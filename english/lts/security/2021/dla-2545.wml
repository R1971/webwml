<define-tag description>LTS security update</define-tag>
<define-tag moreinfo></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8020">CVE-2020-8020</a>

    <p>An improper neutralization of input during web page generation
    vulnerability in open-build-service allows remote attackers to
    store arbitrary JS code to cause XSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8021">CVE-2020-8021</a>

    <p>An improper access control vulnerability in open-build-service
    allows remote attackers to read files of an OBS package where
    the sourceaccess/access is disabled.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.7.1-10+deb9u1.</p>

<p>We recommend that you upgrade your open-build-service packages.</p>

<p>For the detailed security status of open-build-service please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/open-build-service">https://security-tracker.debian.org/tracker/open-build-service</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2545.data"
# $Id: $

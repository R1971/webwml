<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Among others, Andre Heinicke from gpg4win.org found several issues of
nsis, a tool for creating quick and user friendly installers for
Microsoft Windows operating systems.</p>

<p>The issues are fixed by:</p>

<ul>
    <li>using SetDefaultDllDirectories() to restrict implicitly loaded and
        dynamically loaded modules to trusted directories</li>
    <li>creating temporary directories in a way that only elevated users can
        write into it</li>
    <li>not implicitly linking against Version.dll but using wrapper
        functions</li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.46-10+deb8u1.</p>

<p>We recommend that you upgrade your nsis packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1602.data"
# $Id: $

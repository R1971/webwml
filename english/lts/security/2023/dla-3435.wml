<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Cross-site scripting (XSS) vulnerabilities were found in rainloop, a
web-based email client, which could lead to information disclosure
including passphrase leak.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13389">CVE-2019-13389</a>

    <p>It was discovered that RainLoop Webmail lacked XSS protection
    mechanisms such as <code>xlink:href</code> validation, the
    X-XSS-Protection header, and the Content-Security-Policy
    header.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29360">CVE-2022-29360</a>

    <p>Simon Scannell discovered that RainLoop's Email Viewer allows XSS
    via a crafted text/html email message.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.12.1-2+deb10u1.</p>

<p>We recommend that you upgrade your rainloop packages.</p>

<p>For the detailed security status of rainloop please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rainloop">https://security-tracker.debian.org/tracker/rainloop</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3435.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>processCropSelections in tools/tiffcrop.c in LibTIFF, the Tag Image
File Format (TIFF) library and tools, has a heap-based buffer overflow
(e.g., <q>WRITE of size 307203</q>) via a crafted TIFF image.</p>

<p>For Debian 10 buster, this problem has been fixed in version
4.1.0+git191117-2~deb10u6.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>For the detailed security status of tiff please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/tiff">https://security-tracker.debian.org/tracker/tiff</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3297.data"
# $Id: $

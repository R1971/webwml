<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A security vulnerability has been discovered in libhtmlcleaner-java, a Java
HTML parser library. An attacker was able to cause a denial of service
(StackOverflowError) if the parser runs on user supplied input with deeply
nested HTML elements. This update introduces a new nesting depth limit
which can be overridden in cleaner properties.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.21-5+deb10u1.</p>

<p>We recommend that you upgrade your libhtmlcleaner-java packages.</p>

<p>For the detailed security status of libhtmlcleaner-java please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libhtmlcleaner-java">https://security-tracker.debian.org/tracker/libhtmlcleaner-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3520.data"
# $Id: $

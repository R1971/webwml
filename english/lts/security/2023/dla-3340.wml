<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A vulnerability have been found in libgit2, a cross-platform, linkable
library implementation of Git, which may result in remote code execution
when cloning a repository on a NTFS-like filesystem or man-in-the-middle
attacks due to improper verification of cryptographic Signature.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12278">CVE-2020-12278</a>

    <p>An issue was discovered in libgit2 before 0.28.4 and 0.9x before
    0.99.0.  path.c mishandles equivalent filenames that exist because of
    NTFS Alternate Data Streams. This may allow remote code execution when
    cloning a repository.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12279">CVE-2020-12279</a>

    <p>An issue was discovered in libgit2 before 0.28.4 and 0.9x before
    0.99.0.  checkout.c mishandles equivalent filenames that exist because
    of NTFS short names. This may allow remote code execution when cloning a
    repository</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-22742">CVE-2023-22742</a>

    <p>libgit2 is a cross-platform, linkable library implementation of Git.
    When using an SSH remote with the optional libssh2 backend, libgit2 does
    not perform certificate checking by default. Prior versions of libgit2
    require the caller to set the `certificate_check` field of libgit2's
    `git_remote_callbacks` structure - if a certificate check callback is
    not set, libgit2 does not perform any certificate checking. This means
    that by default - without configuring a certificate check callback,
    clients will not perform validation on the server SSH keys and may be
    subject to a man-in-the-middle attack.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
0.27.7+dfsg.1-0.2+deb10u1.</p>

<p>We recommend that you upgrade your libgit2 packages.</p>

<p>For the detailed security status of libgit2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libgit2">https://security-tracker.debian.org/tracker/libgit2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3340.data"
# $Id: $

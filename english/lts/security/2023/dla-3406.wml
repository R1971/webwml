<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in sniproxy, a transparent TLS and HTTP layer 4
proxy with SNI support.
Due to bad handling of wildcard backend hosts, a crafted HTTP or TLS
packet might lead to remote arbitrary code execution.</p>


<p>For Debian 10 buster, this problem has been fixed in version
0.6.0-1+deb10u1.</p>

<p>We recommend that you upgrade your sniproxy packages.</p>

<p>For the detailed security status of sniproxy please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/sniproxy">https://security-tracker.debian.org/tracker/sniproxy</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3406.data"
# $Id: $

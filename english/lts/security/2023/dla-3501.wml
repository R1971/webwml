<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security issues were discovered in renderdoc a stand-alone
graphics debugging tool, which potentially allows a remote attacker
to execute arbitrary code.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33863">CVE-2023-33863</a>

    <p>an integer overflow that results in a heap-based buffer overflow
    that might be exploitable by a remote attacker to execute arbitrary
    code on the machine that runs RenderDoc</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33864">CVE-2023-33864</a>

    <p>an integer underflow that results in a heap-based buffer overflow
    that might be exploitable by a remote attacker to execute arbitrary
    code on the machine that runs RenderDoc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-33865">CVE-2023-33865</a>

    <p>a symlink vulnerability that might be exploitable by a unprivileged
    local attacker to obtain the privileges of the user who runs
    RenderDoc.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
1.2+dfsg-2+deb10u1.</p>

<p>We recommend that you upgrade your renderdoc packages.</p>

<p>For the detailed security status of renderdoc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/renderdoc">https://security-tracker.debian.org/tracker/renderdoc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3501.data"
# $Id: $

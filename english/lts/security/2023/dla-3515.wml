<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>An incorrect Authentication Tag length usage was discovered in cjose, a
C library implementing the Javascript Object Signing and Encryption
(JOSE) standard, which could lead to integrity compromise.</p>

<p>The AES GCM decryption routine incorrectly uses the Tag length from the
actual Authentication Tag as provided in the JSON Web Encryption (JWE)
object, while the <a href="https://www.rfc-editor.org/rfc/rfc7518#section-4.7">specification</a>
says that a fixed length of 16 octets
must be applied.  This could allows an attacker to provide a truncated
Authentication Tag and to modify the JWE accordingly.</p>

<p>For Debian 10 buster, this problem has been fixed in version
0.6.1+dfsg1-1+deb10u1.</p>

<p>We recommend that you upgrade your cjose packages.</p>

<p>For the detailed security status of cjose please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/cjose">https://security-tracker.debian.org/tracker/cjose</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3515.data"
# $Id: $

<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that
may lead to a privilege escalation, denial of service or information
leaks.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-0386">CVE-2023-0386</a>

    <p>It was discovered that under certain conditions the overlayfs
    filesystem implementation did not properly handle copy up
    operations. A local user permitted to mount overlay mounts in user
    namespaces can take advantage of this flaw for local privilege
    escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31436">CVE-2023-31436</a>

    <p>Gwangun Jung reported a a flaw causing heap out-of-bounds
    read/write errors in the traffic control subsystem for the Quick
    Fair Queueing scheduler (QFQ) which may result in information
    leak, denial of service or privilege escalation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-32233">CVE-2023-32233</a>

    <p>Patryk Sondej and Piotr Krysiuk discovered a use-after-free flaw
    in the Netfilter nf_tables implementation when processing batch
    requests, which may result in local privilege escalation for a
    user with the CAP_NET_ADMIN capability in any user or network
    namespace.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
5.10.179-1~deb10u1.</p>

<p>We recommend that you upgrade your linux-5.10 packages.</p>

<p>For the detailed security status of linux-5.10 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux-5.10">https://security-tracker.debian.org/tracker/linux-5.10</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3446.data"
# $Id: $

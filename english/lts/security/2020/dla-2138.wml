<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Similar to <a href="https://security-tracker.debian.org/tracker/CVE-2016-10743">CVE-2016-10743</a> the host access point daemon, hostapd, in
EAP mode used a low quality pseudorandom number generator that leads to
insufficient entropy. The problem was resolved by using the
os_get_random function which provides cryptographically strong pseudo
random data.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
2.3-1+deb8u10.</p>

<p>We recommend that you upgrade your wpa packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2138.data"
# $Id: $

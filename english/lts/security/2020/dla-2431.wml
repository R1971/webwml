<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in the Oniguruma regular
expressions library, notably used in PHP mbstring.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13224">CVE-2019-13224</a>

   <p>A use-after-free in onig_new_deluxe() in regext.c allows
   attackers to potentially cause information disclosure, denial of
   service, or possibly code execution by providing a crafted regular
   expression. The attacker provides a pair of a regex pattern and a
   string, with a multi-byte encoding that gets handled by
   onig_new_deluxe().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16163">CVE-2019-16163</a>

    <p>Oniguruma allows Stack Exhaustion in regcomp.c because of recursion
    in regparse.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19012">CVE-2019-19012</a>

    <p>An integer overflow in the search_in_range function in regexec.c in
    Onigurama leads to an out-of-bounds read, in which the offset of
    this read is under the control of an attacker. (This only affects
    the 32-bit compiled version). Remote attackers can cause a
    denial-of-service or information disclosure, or possibly have
    unspecified other impact, via a crafted regular expression.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19203">CVE-2019-19203</a>

    <p>An issue was discovered in Oniguruma. In the function
    gb18030_mbc_enc_len in file gb18030.c, a UChar pointer is
    dereferenced without checking if it passed the end of the matched
    string. This leads to a heap-based buffer over-read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19204">CVE-2019-19204</a>

    <p>An issue was discovered in Oniguruma. In the function
    fetch_interval_quantifier (formerly known as fetch_range_quantifier)
    in regparse.c, PFETCH is called without checking PEND. This leads to
    a heap-based buffer over-read.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19246">CVE-2019-19246</a>

    <p>Oniguruma has a heap-based buffer over-read in str_lower_case_match
    in regexec.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26159">CVE-2020-26159</a>

    <p>In Oniguruma an attacker able to supply a regular expression for
    compilation may be able to overflow a buffer by one byte in
    concat_opt_exact_str in src/regcomp.c</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
6.1.3-2+deb9u1.</p>

<p>We recommend that you upgrade your libonig packages.</p>

<p>For the detailed security status of libonig please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libonig">https://security-tracker.debian.org/tracker/libonig</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2431.data"
# $Id: $

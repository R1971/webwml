<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A vulnerability was discovered in mailman. GNU Mailman 2.x before 2.1.30
uses the .obj extension for scrubbed application/octet-stream MIME parts.
This behavior may contribute to XSS attacks against list-archive visitors,
because an HTTP reply from an archive web server may lack a MIME type,
and a web browser may perform MIME sniffing, conclude that the MIME type
should have been text/html, and execute JavaScript code.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1:2.1.18-2+deb8u5.</p>

<p>We recommend that you upgrade your mailman packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2200.data"
# $Id: $

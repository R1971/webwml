<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The following CVEs were reported against src:openjpeg2.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12973">CVE-2019-12973</a>

    <p>In OpenJPEG 2.3.1, there is excessive iteration in the
    opj_t1_encode_cblks function of openjp2/t1.c. Remote attackers
    could leverage this vulnerability to cause a denial of service
    via a crafted bmp file. This issue is similar to <a href="https://security-tracker.debian.org/tracker/CVE-2018-6616">CVE-2018-6616</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-6851">CVE-2020-6851</a>

    <p>OpenJPEG through 2.3.1 has a heap-based buffer overflow in
    opj_t1_clbl_decode_processor in openjp2/t1.c because of lack
    of opj_j2k_update_image_dimensions validation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8112">CVE-2020-8112</a>

    <p>opj_t1_clbl_decode_processor in openjp2/t1.c in OpenJPEG 2.3.1
    through 2020-01-28 has a heap-based buffer overflow in the
    qmfbid==1 case, a different issue than <a href="https://security-tracker.debian.org/tracker/CVE-2020-6851">CVE-2020-6851</a>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15389">CVE-2020-15389</a>

    <p>jp2/opj_decompress.c in OpenJPEG through 2.3.1 has a
    use-after-free that can be triggered if there is a mix of
    valid and invalid files in a directory operated on by the
    decompressor. Triggering a double-free may also be possible.
    This is related to calling opj_image_destroy twice.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.1.2-1.1+deb9u5.</p>

<p>We recommend that you upgrade your openjpeg2 packages.</p>

<p>For the detailed security status of openjpeg2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openjpeg2">https://security-tracker.debian.org/tracker/openjpeg2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2277.data"
# $Id: $

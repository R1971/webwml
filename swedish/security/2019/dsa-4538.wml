#use wml::debian::translation-check translation="e41f5efa353b2bdd34609a011c02c9132873e041" mindelta="1"
<define-tag description>säkerhetsuppdatering</define-tag>
<define-tag moreinfo>
<p>Två sårbarheter har upptäckts i implementationen av WPA-protokollet som hittas
i wpa_supplication (station) och hostapd (åtkomstpunkt).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13377">CVE-2019-13377</a>

    <p>Ett timing-baserat sidokanalsangrepp mot WPA3's Dragonfly-handskakning
    vid användning av Brainpool-kurvor kunde användas av en angripare för att få
    lösenordet.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16275">CVE-2019-16275</a>

    <p>Otillräcklig validering av källadressen för vissa mottagna
    Management-frames i hostapd kunde leda till överbelastning för stationer
    som associeras med en åtkomstpunkt. En angripare inom radioavstånd från
    åtkomstpunkten kunde injicera en speciellt skapad icke autentiserad IEEE
    802.11-frame till åtkomstpunkten för att orsaka att associerade stationer
    kan kopplas ifrån och kräva en återanslutning till nätverket.</p></li>

</ul>

<p>För den stabila utgåvan (Buster) har dessa problem rättats i
version 2:2.7+git20190128+0c1e29f-6+deb10u1.</p>

<p>Vi rekommenderar att ni uppgraderar era wpa-paket.</p>

<p>För detaljerad säkerhetsstatus om wpa vänligen se
dess säkerhetsspårare på
<a href="https://security-tracker.debian.org/tracker/wpa">\
https://security-tracker.debian.org/tracker/wpa</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4538.data"

msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2023-07-06 13:17+0200\n"
"Last-Translator: Jean-Paul Guillonneau <guillonneau.jeanpaul@free.fr>\n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: vim\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "Le système d'exploitation universel"

#: ../../english/index.def:12
msgid "DebConf is underway!"
msgstr "DebConf est en cours !"

#: ../../english/index.def:15
msgid "DebConf Logo"
msgstr "Logo de la DebConf"

#: ../../english/index.def:19
msgid "DC22 Group Photo"
msgstr "Photo de groupe de la DC22"

#: ../../english/index.def:22
msgid "DebConf22 Group Photo"
msgstr "Photo de groupe de la DebConf22"

#: ../../english/index.def:26
msgid "Debian Reunion Hamburg 2023"
msgstr "Réunion Debian 2023 à Hambourg"

#: ../../english/index.def:29
msgid "Group photo of the Debian Reunion Hamburg 2023"
msgstr "Photo de groupe de la Réunion Debian 2023 à Hambourg"

#: ../../english/index.def:33
msgid "MiniDebConf Brasília 2023"
msgstr "MiniDebConf 2023 à Brasilia"

#: ../../english/index.def:36
msgid "Group photo of the MiniDebConf Brasília 2023"
msgstr "Photo de groupe de la MiniDebConf 2021 à Brasilia"

#: ../../english/index.def:40
msgid "Mini DebConf Regensburg 2021"
msgstr "MiniDebConf 2021 à Regensburg"

#: ../../english/index.def:43
msgid "Group photo of the MiniDebConf in Regensburg 2021"
msgstr "Photo de groupe de la MiniDebConf 2021 à Regensburg"

#: ../../english/index.def:47
msgid "Screenshot Calamares Installer"
msgstr "Copie d'écran de l'installateur Calamares"

#: ../../english/index.def:50
msgid "Screenshot from the Calamares installer"
msgstr "Copie d'écran de l'installateur Calamares"

#: ../../english/index.def:54 ../../english/index.def:57
msgid "Debian is like a Swiss Army Knife"
msgstr "Debian est comme un couteau suisse"

#: ../../english/index.def:61
msgid "People have fun with Debian"
msgstr "Les gens s'amusent avec Debian"

#: ../../english/index.def:64
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr "Les membres de Debian à la Debconf 18 à Hsinchu s'amusent vraiment"

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "Nouvelles de Debian "

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "Blog"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "Micronouvelles"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "Micronews from Debian"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "Planète"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "The Planet of Debian"

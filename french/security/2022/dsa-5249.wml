#use wml::debian::translation-check translation="8daf1f6bfc7ea2481455a86143754aa03dfc6288" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Lahav Schlesinger a découvert une vulnérabilité dans le greffon de
révocation de strongSwan, une suite IKE/IPsec.</p>

<p>Le greffon de révocation utilise les URI OCSP et les points de
distribution (CDP) qui proviennent de certificats fournis par le terminal
distant. Le greffon ne vérifiait pas la chaîne de confiance du certificat
avant d'utiliser des URI, si bien qu'un attaquant pouvait fournir un
certificat contrefait contenant des URI pointant vers des serveurs sous
leur contrôle, menant éventuellement à des attaques par déni de service.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 5.9.1-1+deb11u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets strongswan.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de strongswan, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/strongswan">\
https://security-tracker.debian.org/tracker/strongswan</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5249.data"
# $Id: $

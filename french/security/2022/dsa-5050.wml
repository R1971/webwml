#use wml::debian::translation-check translation="e29a0fc272d38a01c5817ee1e8419d80e75e9d30" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation de privilèges, un déni de service ou
des fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4155">CVE-2021-4155</a>

<p>Kirill Tkhai a découvert une fuite de données dans la manière dont
l'IOCTL XFS_IOC_ALLOCSP dans le système de fichiers XFS permettait une
augmentation de taille de fichiers ayant une taille non alignée. Un
attaquant pouvait tirer avantage de ce défaut pour une fuite de données sur
le système de fichiers XFS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28711">CVE-2021-28711</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28712">CVE-2021-28712</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28713">CVE-2021-28713</a> (XSA-391)

<p>Juergen Gross a signalé que des dorsaux de PV malveillants peuvent
provoquer un déni de service dans les clients servis par ces dorsaux au
moyen d'événements à haute fréquence, même si ces dorsaux sont exécutés
dans un environnement moins privilégié.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-28714">CVE-2021-28714</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2021-28715">CVE-2021-28715</a> (XSA-392)

<p>Juergen Gross a découvert que les clients Xen peuvent contraindre le pilote
netback de Linux à accaparer une grande quantité de mémoire du noyau, avec
pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-39685">CVE-2021-39685</a>

<p>Szymon Heidrich a découvert une vulnérabilité de dépassement de tampon
dans le sous-système gadget USB, avec pour conséquences la divulgation
d'informations, un déni de service ou une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45095">CVE-2021-45095</a>

<p>Le pilote du protocole Phone Network (PhoNet) présente une fuite de
nombre de références dans la fonction pep_sock_accept().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45469">CVE-2021-45469</a>

<p>Wenqing Liu a signalé un accès mémoire hors limites dans
l'implémentation de f2fs si un inœud a une dernière entrée de xattr non
valable. Un attaquant pouvant monter une image contrefaite pour l'occasion
peut tirer avantage de ce défaut pour un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45480">CVE-2021-45480</a>

<p>Un défaut de fuite de mémoire a été découvert dans la fonction
__rds_conn_create() dans le sous-système du protocole RDS (Reliable
Datagram Sockets).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-0185">CVE-2022-0185</a>

<p>William Liu, Jamie Hill-Daniel, Isaac Badipe, Alec Petridis, Hrvoje
Misetic et Philip Papurt ont découvert un défaut de dépassement de tas dans
la fonction legacy_parse_param dans la fonctionnalité Filesystem Context,
permettant une élévation de privilèges à un utilisateur local (doté de la
capacité CAP_SYS_ADMIN dans l'espace de noms actuel).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-23222">CVE-2022-23222</a>

<p><q>tr3e</q> a découvert que le vérificateur BPF ne restreignait pas
correctement plusieurs types de pointeurs *_OR_NULL permettant à ces types
de faire un pointeur arithmétique. Un utilisateur local avec la capacité
d'appeler bpf() peut tirer avantage de ce défaut pour une élévation de
privilèges. Les appels non privilégiés de bpf() sont désactivés par défaut
dans Debian, atténuant ce défaut.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 5.10.92-1. Cette version inclut des modifications qui sont
destinées à être intégrées dans la prochaine version intermédiaire de
Debian Bullseye.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5050.data"
# $Id: $

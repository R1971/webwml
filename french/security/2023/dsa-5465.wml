#use wml::debian::translation-check translation="599aac57d1b961de1b75b9658ddd2efaffcab5ed" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Seokchan Yoon a découvert que l'absence de vérification dans les
validateurs de courriels et d'URL de Django, un cadriciel en Python de
développement web, pouvait avoir pour conséquence un déni de service.</p>

<p>Pour la distribution oldstable (Bullseye), ce problème a été corrigé
dans la version 2:2.2.28-1~deb11u2. Cette mise à jour corrige aussi les
<a href="https://security-tracker.debian.org/tracker/CVE-2023-23969">CVE-2023-23969</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2023-31047">CVE-2023-31047</a>
et <a href="https://security-tracker.debian.org/tracker/CVE-2023-24580">CVE-2023-24580</a>.</p>

<p>Pour la distribution stable (Bookworm), ce problème a été corrigé dans
la version 3:3.2.19-1+deb12u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de python-django,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/python-django">\
https://security-tracker.debian.org/tracker/python-django</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5465.data"
# $Id: $

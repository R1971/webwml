#use wml::debian::translation-check translation="ee975366116a1b862ff69cc041c1533dd5832e5d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Tavis Ormandy a découvert que, sous des circonstances particulière liées
à la microarchitecture, un registre de vecteur dans les processeurs
<q>Zen 2</q> pouvait ne pas être mis à zéro correctement. Ce défaut
permettait à un attaquant de divulguer le contenu du registre à travers des
processus concurrents, des hyper flux et des clients virtualisés.</p>

<p>Pour plus de détails veuillez consulter
<a href="https://lock.cmpxchg8b.com/zenbleed.html">\
https://lock.cmpxchg8b.com/zenbleed.html</a> et
<a href="https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8">\
https://github.com/google/security-research/security/advisories/GHSA-v6wh-rxpg-cmm8</a>.</p>

<p>Ce problème peut aussi être atténué par une mise à jour du microcode au
moyen du paquet amd64-microcode ou la mise à jour du microprogramme du
système (BIOS/UEFI). Cependant,la version initiale du microcode d'AMD ne
fournit des mises à jour que pour les processeurs EPYC de deuxième
génération : divers processeurs Ryzen sont aussi affectés, mais les mises à
jour ne sont pas encore disponibles.</p>

<p>Pour la distribution stable (Bookworm), ce problème a été corrigé dans
la version 6.1.38-2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5462.data"
# $Id: $

#use wml::debian::translation-check translation="bbf45cabb18dfdd9831a76ce62af71ea9123b9ef" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que les personnes qui utilisent java.sql.Statement ou
java.sql.PreparedStatement dans hsqldb, une base de données SQL Java, pour
traiter des entrées non sûres peuvent être vulnérables à une attaque
d'exécution de code à distance. Par défaut il est permis d'appeler
n'importe quelle méthode statique de toute classe Java dans le classpath
aboutissant à une exécution de code. Le problème peut être évité en mettant
à jour vers la version 2.5.1-1+deb11u1 ou en définissant les propriétés du
système <q>hsqldb.method_class_names</q> pour les classes qu'il est permis
d'appeler. Par exemple, System.setProperty("hsqldb.method_class_names",\
"abc") ou l'argument Java <q>-Dhsqldb.method_class_names="abc"</q> peuvent
être utilisés. À partir de la version 2.5.1-1+deb11u1, toutes les classes
ne sont pas accessibles par défaut, sauf celles présentes dans
java.lang.Math, et doivent être activées manuellement.</p>

<p>Pour la distribution stable (Bullseye), ce problème a été corrigé dans
la version 2.5.1-1+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets hsqldb.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de hsqldb, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/hsqldb">\
https://security-tracker.debian.org/tracker/hsqldb</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5313.data"
# $Id: $

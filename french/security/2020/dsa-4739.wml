#use wml::debian::translation-check translation="e0e83a446207444c8d7cbfe76be73fc5338ccab7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Les vulnérabilités suivantes ont été découvertes dans le moteur web
webkit2gtk :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9862">CVE-2020-9862</a>

<p>Ophir Lojkine a découvert que la copie d'une URL à partir de
l'inspecteur Web pourrait conduire à une injection de commande.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9893">CVE-2020-9893</a>

<p>0011 a découvert qu'un attaquant distant peut être capable de
provoquer la fin inattendue d'une application ou l'exécution de code
arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9894">CVE-2020-9894</a>

<p>0011 a découvert qu'un attaquant distant peut être capable de
provoquer la fin inattendue d'une application ou l'exécution de code
arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9895">CVE-2020-9895</a>

<p>Wen Xu a découvert qu'un attaquant distant peut être capable de
provoquer la fin inattendue d'une application ou l'exécution de code
arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9915">CVE-2020-9915</a>

<p>Ayoub Ait Elmokhtar a découvert que le traitement d'un contenu web
contrefait peut empêcher l'application du « Content Security Policy ».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9925">CVE-2020-9925</a>

<p>Un chercheur anonyme a découvert que le traitement d'un contenu web
contrefait pourrait conduire à une vulnérabilité de script intersite
universel.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 2.28.4-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets webkit2gtk.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de webkit2gtk, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/webkit2gtk">\
https://security-tracker.debian.org/tracker/webkit2gtk</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4739.data"
# $Id: $

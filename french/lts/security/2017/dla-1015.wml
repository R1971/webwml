#use wml::debian::translation-check translation="e03db691eef0bf1e048da79524a1cbc851b57faf" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il existait une vulnérabilité de divulgation de clef dans libgcrypt11, une
bibliothèque de routines de chiffrement.</p>

<p>Il est bien connu que l’implémentation en temps constant d’exponentiation
modulaire ne peut pas utiliser des fenêtres dynamiques. Cependant, les
bibliothèques logicielles telles que Libgcrypt, utilisée par GnuPG, continuent
d’utiliser des fenêtres dynamiques. Il est largement admis que, même si le
modèle complet des carrés et multiplications est observé à travers une attaque
par canal auxiliaire, le nombre de bits d’exposant divulgué n’est pas suffisant
pour conduire à une récupération complète de clef à l’encontre de RSA. En
particulier, des fenêtres dynamiques de quatre bits divulguent seulement 40 %
des bits, et celles de cinq bits en divulguent seulement 33%.</p>

<p>-- Sliding right into disaster : Left-to-right sliding windows leak<br>
 <url "https://eprint.iacr.org/2017/627"></p>

<p>Pour Debian 7 <q>Wheezy</q>, ce problème a été corrigé dans la
version 1.5.0-5+deb7u6 de libgcrypt11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libgcrypt11.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1015.data"
# $Id: $

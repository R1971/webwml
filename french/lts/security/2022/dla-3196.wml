#use wml::debian::translation-check translation="25b5ce5d942ef94cd18f5c9ac6fbfd0a697ac22e" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans Thunderbird. Cela
pouvait éventuellement avoir pour conséquences l'exécution de code
arbitraire ou la divulgation d'informations, une usurpation ou le
contournement de la politique de cookie <q>SameSite</q>.</p>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
1:102.5.0-1~deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets thunderbird.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de thunderbird, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/thunderbird">\
https://security-tracker.debian.org/tracker/thunderbird</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3196.data"
# $Id: $

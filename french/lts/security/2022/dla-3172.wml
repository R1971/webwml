#use wml::debian::translation-check translation="23127f315f0734d770fa742ea84fe6feb806a81a" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>libxml2, la bibliothèque XML GNOME, était vulnérable à des dépassements
d'entier et à une corruption de mémoire.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40303">CVE-2022-40303</a>

<p>L'analyse d'un document XML avec l'option XML_PARSE_HUGE activée peut
avoir pour conséquence un dépassement d'entier du fait de l'absence de
vérification de sécurité dans certaines fonctions. De même, la fonction
xmlParseEntityValue n'avait plus aucune limitation de longueur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40304">CVE-2022-40304</a>

<p>Quand une boucle de références est détectée dans la fonction de
nettoyage d'entités XML, les données de l'entité XML peuvent être stockées
dans un dictionnaire. Dans ce cas, le dictionnaire devient corrompu avec
pour conséquences des erreurs de logiques, dont des erreurs de mémoires
telles qu'une double libération de zone mémoire.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
2.9.4+dfsg1-7+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libxml2.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libxml2, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxml2">\
https://security-tracker.debian.org/tracker/libxml2</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3172.data"
# $Id: $

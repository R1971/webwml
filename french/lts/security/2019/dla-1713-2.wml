#use wml::debian::translation-check translation="d105daff96c67406bb53e5065f502d586be56c74" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de libsdl1.2 publiée dans la DLA 1713-1 conduisait à une
regression, provoquée par un correctif incomplet pour
<a href="https://security-tracker.debian.org/tracker/CVE-2019-7637">CVE-2019-7637</a>.
Ce problème était connu de l’amont et aboutissait, entre autres, dans des
versions de fenêtre issues de libsdl1.2 échouant à définir le mode vidéo.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 1.2.15-10+deb8u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libsdl1.2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1713-2.data"
# $Id: $

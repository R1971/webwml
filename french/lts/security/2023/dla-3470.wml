#use wml::debian::translation-check translation="aba4d501b9b28a01b27b1c2907e9de5986a01f01" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dans OWSLib, une bibliothèque cliente en Python pour les services web d’Open
Geospatial, l'analyseur XML ne désactivait pas la résolution d’entités, ce qui
pouvait conduire à une lecture de fichiers arbitraires à partir d’une charge
utile XML contrôlée par l’attaquant.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 0.17.1-1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets owslib.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de owslib,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/owslib">\
https://security-tracker.debian.org/tracker/owslib</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3470.data"
# $Id: $

#use wml::debian::translation-check translation="c283aeb2b734e213478aafe5017d0c7bce0ec547" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été trouvés dans opensc, un ensemble de
bibliothèques et d’utilitaires pour accéder à une carte à puce, qui pouvaient
conduire à un plantage d'application ou à une fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6502">CVE-2019-6502</a>

<p>Dhiraj Mishra a découvert une fuite mineure de mémoire dans l’utilitaire
en ligne de commande <code>eidenv(1)</code> lors d’un cas d’erreur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42779">CVE-2021-42779</a>

<p>Une vulnérabilité d’utilisation de tas après libération a été découverte dans
<code>sc_file_valid()</code>.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42780">CVE-2021-42780</a>

<p>Une vulnérabilité d’utilisation après libération a été découverte dans
<code>insert_pin()</code>, qui pouvait éventuellement planter les programmes
en utilisant la bibliothèque.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42781">CVE-2021-42781</a>

<p>Plusieurs vulnérabilités de dépassement de tampon de tas ont été découvertes
dans <code>pkcs15-oberthur.c</code>, qui pouvaient éventuellement planter les
programmes utilisant la bibliothèque.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-42782">CVE-2021-42782</a>

<p>Plusieurs vulnérabilités de dépassement de tampon de tas ont été découvertes
dans divers endroits, qui pouvaient éventuellement planter les programmes
utilisant la bibliothèque.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2977">CVE-2023-2977</a>

<p>Une vulnérabilité de dépassement de tampon a été découverte dans
<code>cardos_have_verifyrc_package()</code> de pkcs15, qui pouvait conduire à
un plantage ou à une fuite d'informations à l’aide d’un paquet de carte à puce
avec un contexte ASN1 malveillant.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 0.19.0-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets opensc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de opensc,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/opensc">\
https://security-tracker.debian.org/tracker/opensc</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3463.data"
# $Id: $

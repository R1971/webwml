#use wml::debian::translation-check translation="0da107e08419173d14038756772da3498ced0fe8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une nouvelle version 10.3.38 d’entretien mineur de MariaDB a été publiée.
Elle inclut des correctifs pour un problème majeur de performance et
consommation mémoire (MDEV-29988).</p>

<p>Pour de plus amples détails, consultez les
<a href="https://mariadb.com/kb/en/mariadb-10-3-38-release-notes/">notes de publication de MariaDB 10.3.38</a>.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:10.3.38-0+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mariadb-10.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mariadb-10.3,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mariadb-10.3">\
https://security-tracker.debian.org/tracker/mariadb-10.3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3337.data"
# $Id: $

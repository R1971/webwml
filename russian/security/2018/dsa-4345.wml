#use wml::debian::translation-check translation="1d1f8d159bd57a26b5a8603a6dfc4a1937981b1c" mindelta="1" maintainer="Lev Lamberov"
<define-tag description>обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В Samba, файловом сервере, сервере печати и аутентификации SMB/CIFS
для Unix, было обнаружено несколько уязвимостей. Проект Common Vulnerabilities and
Exposures определяет следующие проблемы:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14629">CVE-2018-14629</a>

    <p>Флориан Штойлпнер обнаружил, что Samba уязвима к бесконечным рекурсивным
    запросам, вызываемым циклами CNAME, что приводит к
    отказу в обслуживании.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-14629.html">\
    https://www.samba.org/samba/security/CVE-2018-14629.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16841">CVE-2018-16841</a>

    <p>Алекс Маккаиш обнаружил, что пользователь, имеющий действительный сертификат
    или смарт-карту, может аварийно завершить Samba AD DC KDC в случае, если указанная
    служба настроена на принятие аутентификации с помощью смарт-карт.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-16841.html">\
    https://www.samba.org/samba/security/CVE-2018-16841.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16851">CVE-2018-16851</a>

    <p>Гарминг Сэм из команды Samba и Catalyst обнаружил разыменование NULL-указателя
    в LDAP-сервере Samba AD DC, позволяющее пользователю, способному
    считывать более 256МБ записей LDAP, аварийно завершать работу LDAP-сервера
    Samba AD DC.</p>

    <p><a href="https://www.samba.org/samba/security/CVE-2018-16851.html">\
    https://www.samba.org/samba/security/CVE-2018-16851.html</a></p></li>

</ul>

<p>В стабильном выпуске (stretch) эти проблемы были исправлены в
версии 2:4.5.12+dfsg-2+deb9u4.</p>

<p>Рекомендуется обновить пакеты samba.</p>

<p>С подробным статусом поддержки безопасности samba можно ознакомиться на
соответствующей странице отслеживания безопасности по адресу
<a href="https://security-tracker.debian.org/tracker/samba">\
https://security-tracker.debian.org/tracker/samba</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4345.data"

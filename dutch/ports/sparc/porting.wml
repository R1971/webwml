#use wml::debian::template title="Debian SPARC -- Overzettingsdocumentatie" NOHEADER="yes"
#include "$(ENGLISHDIR)/ports/sparc/menu.inc"
#use wml::debian::translation-check translation="f9d5abd797e762089776545824869e3e44bd2c42"

<h1>Documentatie over de overzetting van Debian naar SPARC</h1>

<h2>Debian-pakketten overzetten naar SPARC</h2>
  <p>
Als u een officiële medewerker wilt zijn van het geschikt maken van Debian voor
SPARC, moet u een geregistreerde Debian-ontwikkelaar zijn. Dat wil zeggen dat uw
publieke sleutel moet voorkomen in de officiële sleutelbos.
  <p>
De inspanningen voor het overdragen van Debian naar SPARC zijn nu georganiseerd
rond het uitstekende <code>wanna-build</code>-systeem dat voor het eerst
gebruikt werd voor het overdragen van Debian naar <a
href="../m68k/">m68k</a>. Met het bestaan van <code>wanna-build</code> komt
het overdragen neer op het opsporen van de pakketten waarvoor de automatische
compilatie faalde, om dit dan te gaan onderzoeken om te kunnen bepalen wat er
fout ging.
  <p>
Logboeken van mislukte compilaties zijn te vinden op
<a href="https://buildd.debian.org/status/architecture.php?a=sparc64">de
webpagina's van de buildd van SPARC 64</a>.
Ook kunt u <code>wanna-build</code> e-mailen en vragen naar de logboeken van de
mislukte compilaties (zie het bestand <code>README.mail</code> uit de
<code>wanna-build</code>-distributie).
  <p>
Ernstige overzetters zouden moeten leren hoe ze kunnen omgaan met
<code>wanna-build</code> via e-mail. U zult <a
href="mailto:bcollins@debian.org">Ben Collins
&lt;bcollins@debian.org&gt;</a> moeten vragen om uw publieke sleutel toe te
voegen aan de lijst met bekende sleutels.
  <p>
Alle ontwikkelaars van Debian kunnen gebruik maken van Debian's
<a href="https://db.debian.org/machines.cgi">overzettingscomputers</a> om
hun pakketten te testen op de SPARC-architectuur.


<h2>Ik ben geen officiële ontwikkelaar; kan ik toch helpen?</h2>
  <p>
Zeker. In feite vereist het meeste van het echte werk in een overzetting van
Debian geen officiële status, alleen kennis. Er zijn een aantal dingen die u
kunt doen:
<ul>
      <li>
Bugs opsporen, en ze rapporteren aan het <a href="$(HOME)/Bugs/">bugvolgsysteem
van Debian</a>.
      <li>
Patches uitwerken voor bekende bugs. Zorg ervoor dat u de patches verzendt naar het bugvolgsysteem!
      <li>
Helpen met documentatie. De meeste documentatiegebieden worden beheerd onder CVS
en de meeste auteurs van documentatie kunnen CVS-toegang verlenen aan
overzetters die geen officiële status hebben en die willen helpen.
    </ul>
  <p>
Dus ga uw gang en stuur een e-mail naar <a
href="mailto:debian-sparc@lists.debian.org">&lt;debian-sparc@lists.debian.org&gt;</a>
met een beschrijving van hoe u zou willen helpen. We zijn er zeker van dat er
iemand is die u op weg kan helpen.


# <!-- Keep this comment at the end of the file
# Local variables:
# mode: sgml
# sgml-indent-data:nil
# sgml-doctype:"../../releases/.doctype"
# End:
# -->

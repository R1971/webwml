#use wml::debian::template title="HTML-gebruik door de webpagina's van Debian" BARETITLE=true
#use wml::debian::common_tags
#use wml::debian::acronyms
#use wml::debian::toc
#use wml::debian::translation-check translation="648de6ad5bea60540e41a5733da0b761a34c7927"

<p>
Deze pagina is nog een concept.
</p>

<toc-display/>

<toc-add-entry name="preface">Voorwoord</toc-add-entry>

<p>Deze pagina is gemaakt om redacteuren en vertalers te helpen bij het maken
van pagina's waarin tags op een goede manier gebruikt worden. Ze bevat hints
over het gebruik van tags en hoe u nieuwe pagina's kunt maken en deze
gemakkelijker kunt vertalen.</p>


<toc-add-entry name="general">Enkele algemene wenken</toc-add-entry>
<p>Voor nieuwe pagina's of vertalingen volgt hier een lijst met algemene
adviezen</p>
<dl>
<dt>gebruik geen lange regels</dt>
<dd>
De wml-bestanden en andere bestanden moeten regels hebben die in een normaal
terminalvenster passen. Dit is gemakkelijker te bewerken in vi, beter te
doorzoeken en gemakkelijker te vertalen. Dit is ook belangrijk omdat het
moeilijker is om allerlei problemen op te lossen in lange regels.
</dd>
<dt>houd indien mogelijk tags op afzonderlijke regels</dt>
<dd>
De meeste HTML-tags kunnen op afzonderlijke regels worden gehouden. Enkele
daarvan zijn: &lt;div&gt;, &lt;p&gt;, &lt;table&gt;, &lt;ul&gt;. Om het de
vertalers gemakkelijker te maken, moet u alle tags die op deze manier kunnen
worden gebruikt, op aparte regels houden. Anders kunnen vertalers per ongeluk
tags verwijderen en vergeten ze na het vertalen terug te plaatsen.
</dd>
<dt>gebruik geen spaties of regeleinden in tags die binnenin een tekstregel
moeten staan</dt>
<dd>Sommige tags produceren een spatie als ze op een aparte regel staan. Een
ervan is de tag &lt;q&gt; die voor kleine aanhalingen of citaten gebruikt
wordt. U mag ze alleen scheiden op een regel in één geheel samen met inhoud.
Anders zou er in de HTML-pagina achteraf een spatie kunnen staan tussen inhoud
en tag. Tussen woorden binnen deze tags mag u zoveel regeleinden of spaties
hebben als u wil.
</dd>
</dl>

<toc-add-entry name="abbreviations">Afkortingen en acroniemen</toc-add-entry>
<p>
Voor afkortingen en acroniemen moet de HTML-tag &lt;acronym&gt; worden
gebruikt. Er zijn twee redenen waarom het gebruik van de tag &lt;abbr&gt; niet
wordt aanbevolen: ten eerste ondersteunen niet alle browsers dit en ten tweede
zijn de definities over wat een acroniem en wat een afkorting is, inconsistent.
</p>
<p>
Een acroniem wordt aan de pagina toegevoegd in de volgende syntaxis:
<code>&lt;acronym lang="taal-code" title="Volledige definitie van het
acroniem"&gt;ACRONIEM&lt;/acronym&gt;</code>. De titel bevat de volledige
woorden zoals in de spreektaal. Indien het acroniem is opgebouwd uit de
beginletters van woorden, moeten die letters in de titel in hoofdletter staan.
Het attribuut lang is alleen nodig als het acroniem of de afkorting uit een
vreemde taal afkomstig zijn.
</p>
<p>
Er is al een reeks veelgebruikte acroniemen opgenomen in de wml-sjablonen om
deze in uw pagina te gebruiken; u moet een regel toevoegen om
<code>acronyms</code> te gebruiken in het wml-bestand. Bijvoorbeeld de wml-tag
voor DD is &lt;acronym_DD /&gt;.
</p>

<toc-add-entry name="citations">Citaten en aanhalingen</toc-add-entry>
<p>
Er zijn verschillende regels over wat in verschillende talen een citaat of een
aanhaling is. Als u een kort citaat in de tekstregel heeft, moet u de tag
&lt;q&gt; gebruiken. De weergave van de inhoud wordt afgehandeld door de taal
CSS. &lt;q&gt;-tags mogen geen spatie of regeleinde hebben tussen de openende
en sluitende tag en de inhoud.
</p>
<p>
Voor langere citaten wordt de tag &lt;blockquote&gt; gebruikt. Een
&lt;blockquote&gt; omsluit een of meer alinea's tekst, die worden gemarkeerd
met &lt;p&gt;. Gebruik de &lt;blockquote&gt;-tags niet voor het centreren van
een tekstblok dat geen citaat is. Blockquotes zijn uitsluitend bedoeld voor
citaten en zullen in de toekomst worden weergegeven door taalspecifieke
CSS-code.
</p>
<p>
Er bestaat in HTML ook een tag &lt;cite&gt;. De tag &lt;cite&gt; wordt niet
gebruikt voor de geciteerde tekst zelf. Hij wordt gebruikt voor de bron van een
citaat. Dit kan de naam zijn van de persoon van wie het citaat afkomstig is en
wordt in de vorm van een URL als attribuut toegevoegd aan een
&lt;blockquote&gt;.
</p>

<toc-add-entry name="code">Namen van programma's en code</toc-add-entry>
<p>
Voor namen van programma's en computercode is er een tag genaamd &lt;code&gt;.
Browsers weten normaal gesproken hoe ze code en namen van programma's moeten
weergeven, maar de weergave kan ook aangepast worden met CSS. Het is geen goed
idee om in plaats daarvan &lt;tt&gt; te gebruiken, omdat dit niets zegt over de
inhoud.
</p>

<toc-add-entry name="samp">Voorbeelden van computeruitvoer</toc-add-entry>
<p>
Voor computeruitvoer op het scherm is er een speciale tag genaamd &lt;samp&gt;.
Als u een groter blok computeruitvoer heeft, moet u ook eens in
het CSS-bestand kijken of er geen speciale klasse voor bestaat.
</p>

<toc-add-entry name="kbd">Invoer vanuit het toetsenbord</toc-add-entry>
<p>Als er voorbeelden zijn waarbij de gebruiker iets op het toetsenbord moet
typen, wordt de tag &lt;kbd&gt; gebruikt voor de invoer van de gebruiker. Zie
ook het hoofdstuk over <a href="#var">variabelen</a> voor de wijze waarop het
invoeren van variabelen getagd wordt.
</p>

<toc-add-entry name="var">Variabelen</toc-add-entry>
<p>
Soms is het nodig om de nadruk te leggen op de invoer van een variabele, zoals
een speciaal IP-adres of de gebruikersnaam die moet worden gegeven in een
programma-aanroep op de commandoregel. Voor een dergelijke invoer van een
variabele wordt de tag &lt;var&gt; gebruikt.
</p>

<toc-add-entry name="pre">Vooraf opgemaakte inhoud</toc-add-entry>
<p>
De tag &lt;pre&gt; wordt alleen gebruikt voor vooraf opgemaakte tekst.
Regellengte, spaties en andere zaken blijven behouden. Uiteraard kan deze tag
de meeste andere HTML-tags niet bevatten.
</p>

<toc-add-entry name="img">Afbeeldingen</toc-add-entry>
<p>
Als er afbeeldingen worden toegevoegd op de pagina, is het niet nodig om een
ongeldige border=0 toe te voegen als attribuut. Maar indien mogelijk moeten de
afbeeldingsgrootte en een <code>alt</code>-attribuut worden toegevoegd. Indien
de grootte niet vermeld wordt, wordt deze toegevoegd door wml, maar dat vergt
compilatietijd. Het attribuut <code>alt</code> moet iets bevatten dat
gebruikers die met lynx browsen en blinden vertelt wat er in de afbeelding
staat.
</p>


# <toc-add-entry name=""></toc-add-entry>


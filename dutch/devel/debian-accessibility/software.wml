#use wml::debian::template title="Toegankelijk Debian - Software"
#use wml::debian::translation-check translation="a1eaa8012e6459917b80adb073e753fab27bfdb5"
{#style#:<link rel="stylesheet" href="style.css" type="text/css" />:#style#}

<define-tag a11y-pkg endtag=required>
<preserve name tag url/>
<set-var %attributes>
<h3><if "<get-var url>"
        <a href="<get-var url>" name="<get-var tag>"><get-var name></a>
      <a href="https://packages.debian.org/<get-var tag>" name="<get-var tag>"><get-var name></a>></h3>
  %body
<restore name tag url/>
</define-tag>

<h2><a id="speech-synthesis" name="speech-synthesis">Spraaksynthese en daarmee
verband houdende API's</a></h2>

<p>
  Een uitgebreide lijst is beschikbaar op de
  <a href="https://blends.debian.org/accessibility/tasks/speechsynthesis">taakpagina voor spraaksynthese</a>
</p>

<a11y-pkg name="EFlite" tag=eflite url="http://eflite.sourceforge.net/">
<p>
  Een spraakserver voor <a href="#emacspeak">Emacspeak</a> en
  <a href="#yasr">yasr</a> (of andere schermlezers) waarmee deze
  kunnen communiceren met <a href=" #flite">Festival Lite</a>, een gratis
  tekst-naar-spraak-mechanisme, ontwikkeld in het CMU Speech Center als een
  afgeleide van <a href="#festival">Festival</a>.
</p>
<p>
  Vanwege beperkingen die het overkrijgt van zijn backend, biedt EFlite
  momenteel alleen ondersteuning voor de Engelse taal.
</p>
</a11y-pkg>
<a11y-pkg name="eSpeak" tag=espeak>
<p>
eSpeak/eSpeak-NG is een op software gebaseerde spraaksynthesizer voor Engels en
enkele andere talen.
</p>
<p>
eSpeak produceert Engelse spraak van goede kwaliteit. Het gebruikt een andere
synthesemethode dan andere openbron tekst-naar-spraak-mechanismes (TTS) (geen
spraaksynthese door concatenatie, waardoor het ook een zeer kleine voetafdruk
heeft), en klinkt heel anders. Het is misschien niet zo natuurlijk of
<q>vloeiend</q>, maar sommigen vinden de articulatie helderder en gemakkelijker
om er langere tijd naar te luisteren.
</p>
<p>
Het kan worden uitgevoerd als een programma aan de opdrachtregel om tekst uit
een bestand of uit stdin voor te lezen. Het werkt ook goed als een
<q>Spreker</q> met het KDE tekst-naar-spraak systeem (KTTS), bijvoorbeeld als
een alternatief voor <a href="#festival">Festival</a>. Als zodanig kan het
tekst voorlezen die is geselecteerd in het klembord, of rechtstreeks vanuit de
Konqueror-browser of de Kate-editor.
</p>
  <ul>
    <li>Bevat verschillende stemmen waarvan de kenmerken kunnen worden
    aangepast.</li>
    <li>Kan spraakuitvoer als een WAV-bestand produceren.</li>
    <li>Kan tekst omzetten naar foneemcodes, zodat het kan worden omgebouwd tot
    frontend voor een ander spraaksynthesemechanisme.</li>
    <li>Potentieel voor andere talen. Rudimentaire (en waarschijnlijk
    humoristisch bedoelde) pogingen tot Duits en Esperanto zijn inbegrepen.</li>
    <li>Compacte omvang. Het programma en zijn data zijn samen ongeveer 350
    kbytes groot.</li>
    <li>Geschreven in C++.</li>
  </ul>
<p>
eSpeak kan ook gebruikt worden met <a href="#speech-dispatcher">Speech Dispatcher</a>.
</p>
</a11y-pkg>
<a11y-pkg name="Festival Lite" tag=flite>
<p>
  Een klein snel runtime spraaksynthesemachanisme. Het is de recentste
  toevoeging aan de suite van vrije op software gebaseerde
  synthesegereedschappen, waaronder het spraaksynthesesysteem Festival van de
  Universiteit van Edinburgh en het FestVox-project van de Carnegie
  Mellon-universiteit, dat gereedschappen, scripts en documentatie voor het
  bouwen van synthetische stemmen bevat. Nochtans heeft flite zelf geen van
  beide systemen nodig om te functioneren.
</p>
<p>
  Het ondersteunt momenteel enkel de Engelse taal.
</p>
</a11y-pkg>
<a11y-pkg name="Festival" tag="festival"
          url="http://www.cstr.ed.ac.uk/projects/festival/">
<p>
  Een algemeen meertalig spraaksynthesesysteem, ontwikkeld aan het
  <a href="http://www.cstr.ed.ac.uk/">CSTR</a> [<i>C</i>entrum voor
  <i>S</i>raak-<i>T</i>echnologie-<i>R</i>esearch] van de
  <a href="http://www.ed.ac.uk/text.html">Universiteit van Edinburgh</a>.
</p>
<p>
  Festival biedt een volledig tekst-naar-spraaksysteem met diverse API's,
  alsmede een omgeving voor ontwikkeling en onderzoek van
  spraaksynthesetechnieken. Het is geschreven in C++ met een op Scheme
  gebaseerde commando-interpreter voor algemeen beheer.
</p>
<p>
  Naast voor onderzoek naar spraaksynthese is festival ook bruikbaar als op
  zichzelf staand programma voor spraaksynthese. Het is in staat om duidelijk
  verstaanbare spraak uit tekst te produceren.
</p>
</a11y-pkg>
<a11y-pkg name="Speech Dispatcher" tag="speech-dispatcher"
          url="http://www.freebsoft.org/speechd">
<p>
  Biedt een apparaatonafhankelijke laag voor spraaksynthese. Het ondersteunt
  verschillende software- en hardware-spraaksynthesizers als backend en biedt
  een generieke laag voor het synthetiseren van spraak en het afspelen van
  PCM-gegevens via die verschillende backends naar toepassingen.
</p>
<p>
  Verschillende concepten van hogere orde, zoals buffering versus
  onderbrekingen in de spraak en toepassingsspecifieke gebruikersconfiguraties
  worden apparaatonafhankelijk geïmplementeerd, waardoor de programmeur van de
  toepassing niet opnieuw het wiel hoeft uit te vinden.
</p>
</a11y-pkg>


<h2><a name="i18nspeech">Geïnternationaliseerde spraaksynthese</a></h2>
<p>
Alle momenteel beschikbare vrije oplossingen voor op software gebaseerde
spraaksynthese lijken één gemeenschappelijke tekortkoming te hebben: ze zijn
meestal beperkt tot het Engels en bieden slechts zeer marginale ondersteuning
voor andere talen, of in de meeste gevallen helemaal geen.
Van alle vrije op software gebaseerde spraaksynthesizers voor Linux ondersteunt
alleen CMU Festival meer dan één natuurlijke taal. CMU Festival kan Engels,
Spaans en Welsh synthetiseren. Duits wordt niet ondersteund. Frans wordt niet
ondersteund. Russisch wordt niet ondersteund. Als internationalisering en
lokalisering de trends zijn in software en webdiensten, is het dan redelijk om
van blinden die geïnteresseerd zijn in Linux te eisen dat zij Engels leren,
alleen maar om de output van hun computer te begrijpen, en al hun
correspondentie in een vreemde taal te voeren?
</p>
<p>
Helaas is spraaksynthese niet echt het favoriete persoonlijke project van
ontwikkelaar Jan Modaal. Het maken van een begrijpelijke op software gebaseerde
spraaksynthesizer brengt tijdrovende taken met zich mee.
Concatenatieve spraaksynthese vereist de zorgvuldige aanleg van een
foneemdatabase die alle mogelijke combinaties van klanken voor de doeltaal
bevat. Regels die de omzetting van de tekstrepresentatie in afzonderlijke
fonemen bepalen, moeten ook worden ontwikkeld en verfijnd, waarbij meestal de
opdeling van de stroom tekens in logische groepen zoals zinnen, zinsdelen en
woorden nodig is. Een dergelijke lexicale analyse vereist een taalspecifiek
lexicon dat zelden onder een vrije licentie wordt vrijgegeven.
</p>
<p>
Een van de meest veelbelovende spraaksynthesesystemen is Mbrola, met
foneemdatabases voor meer dan enkele tientallen verschillende talen. De
synthese zelf is vrije software. Helaas zijn de foneemdatabases alleen voor
niet-militair en niet-commercieel gebruik. Het ontbreekt ons aan vrije
foneem-databases voor gebruik in het Debian besturingssysteem.
</p>
<p>
Zonder een algemene meertalige op software gebaseerde spraaksynthesizer kan
Linux niet worden aanvaard door aanbieders van ondersteunende technologie en
mensen met een visuele handicap. Wat kunnen we doen om dit te verbeteren?
</p>
<p>
Er zijn in principe twee benaderingen mogelijk:
</p>
<ol>
<li>Een groep mensen optrommelen die hierbij willen helpen, en proberen de
situatie actief te verbeteren. Dit kan een beetje ingewikkeld worden, aangezien
er veel specifieke kennis over spraaksynthese nodig is, wat niet zo gemakkelijk
is als het via een autodidactische aanpak gebeurt. Dit zou u evenwel niet
moeten ontmoedigen. Als u denkt dat u een groep mensen kunt motiveren die groot
genoeg is om enkele verbeteringen te bereiken, zou het de moeite waard zijn om
dat te doen.
</li>
<li>Financiering vinden en een instituut inhuren dat al over de knowhow
beschikt om de nodige foneemdatabases, lexica en transformatieregels te
creëren. Deze aanpak heeft het voordeel dat de kans groter is dat hij
kwaliteitsvolle resultaten oplevert, en hij zou ook veel eerder dan de eerste
aanpak tot verbeteringen moeten leiden. Uiteraard moet de licentie waaronder al
het resulterende werk wordt vrijgegeven vooraf worden overeengekomen, en moet
deze voldoen aan de DFSG-vereisten. De ideale oplossing zou natuurlijk zijn om
een universiteit te overtuigen om op eigen kosten zo'n project te ondernemen en
de resultaten ervan als bijdrage te leveren aan de Vrije Softwaregemeenschap.
</li>
</ol>
<p>
Ook niet onbelangrijk is dat de meeste commercieel succesvolle
spraaksyntheseproducten tegenwoordig geen gebruik meer lijken te maken van
concatenatieve spraaksynthese, voornamelijk omdat de geluidsdatabases veel
schijfruimte in beslag nemen. Dit is niet echt wenselijk voor kleine ingebedde
producten, zoals bijvoorbeeld spraak op een mobiele telefoon. Recent
uitgebrachte vrije software zoals <a href="#espeak">eSpeak</a> lijken deze
benadering te willen proberen, welke zeer de moeite waard kan zijn om te
bekijken.
</p>


<h2><a id="emacs" name="emacs">Schermweergave-uitbreidingen voor Emacs</a></h2>
<a11y-pkg name="Emacspeak" tag="emacspeak"
          url="http://emacspeak.sourceforge.net/">
<p>
  Een systeem voor spraakuitvoer waarmee iemand die niet kan zien, rechtstreeks
  op een UNIX-systeem kan werken. Eens u Emacs met Emacspeak daarin geladen
  opstart, krijgt u gesproken feedback over alles wat u doet. Hoever u daarmee
  komt zal afhangen van hoe goed u Emacs kunt gebruiken. Er is niets dat u niet
  binnen Emacs kunt doen :-). Dit pakket bevat spraakservers geschreven in tcl
  ter ondersteuning van de spraaksynthesizers DECtalk Express en DECtalk
  MultiVoice. Kijk voor andere synthesizers uit naar aparte
  spraakserverpakketten zoals Emacspeak-ss of <a href="#eflite">eflite</a>.
</p>
</a11y-pkg>
<a11y-pkg name="speechd-el" tag="speechd-el"
          url="http://www.freebsoft.org/speechd-el">
<p>
  Emacs-client voor spraaksynthesizers, brailleleesregels en andere
  alternatieve uitvoerinterfaces. Het biedt een volledige spraak- en
  braille-uitvoeromgeving voor Emacs. Het is vooral bedoeld voor gebruikers met
  een visuele beperking die niet-visuele communicatie met Emacs nodig hebben,
  maar het kan gebruikt worden door iedereen die behoefte heeft aan
  geavanceerde spraak- of een ander soort alternatieve uitvoer van Emacs.
</p>
</a11y-pkg>


<h2><a id="console" name="console">Consoleschermlezers (tekstmodus)</a></h2>

<p>
  Een uitgebreide lijst is beschikbaar op de
  <a href="https://blends.debian.org/accessibility/tasks/console">taakpagina
  voor consoleschermlezers</a>
</p>

<a11y-pkg name="BRLTTY" tag="brltty" url="https://brltty.app/">
<p>
  Een achtergronddienst die een blinde persoon toegang geeft tot de
  Linux-console met behulp van een zachte brailleleesregel.
  Hij stuurt de braille-terminal aan en biedt volledige functionaliteit voor
  schermweergave.
</p>
<p>
  De brailleapparaten die door BRLTTY worden ondersteund, staan vermeld in de
  <a href="https://brltty.app/doc/KeyBindings/#braille-device-bindings">
  apparaatdocumentatie van BRLTTY-</a>
</p>
<p>
  BRLTTY biedt ook een client/server-infrastructuur voor toepassingen die
  gebruik willen maken van een brailleleesregel. Het achtergronddienstproces
  luistert naar inkomende TCP/IP-verbindingen op een bepaalde poort. Een
  gedeelde objectbibliotheek voor clients is beschikbaar in het pakket
  <a href="https://packages.debian.org/libbrlapi">libbrlapi</a>. Een statische
  bibliotheek, header-bestanden en documentatie zijn beschikbaar in het pakket
  <a href="https://packages.debian.org/libbrlapi-dev">libbrlapi-dev</a>. Deze
  functionaliteit wordt bijvoorbeeld gebruikt door <a href="#gnome-orca">Orca</a>
  om ondersteuning te bieden voor schermtypes die nog niet rechtstreeks
  ondersteund worden door Gnopernicus.
</p>
</a11y-pkg>
<a11y-pkg name="Yasr" tag="yasr" url="http://yasr.sourceforge.net/">
<p>
  Een universele consoleschermlezer voor GNU/Linux en andere UNIX-achtige
  besturingssystemen. De naam <q>yasr</q> is een afkorting die kan staan voor
  zowel <q>Yet Another Screen Reader</q> (nog een andere schermlezer) als voor
  <q>Your All-purpose Screen Reader</q> (uw universele schermlezer).
</p>
<p>
  Momenteel tracht yasr de hardware-synthesizers Speak-out, DEC-talk, BNS,
  Apollo, en DoubleTalk te ondersteunen. Het kan ook communiceren met
  Emacspeak spraakservers en kan dus gebruikt worden met niet rechtstreeks
  ondersteunde synthesizers, zoals <a href="#flite">Festival Lite</a> (via
  <a href="#eflite">eflite</a>) of FreeTTS.
</p>
<p>
  Yasr werkt door een pseudo-terminal te openen en een shell uit te voeren,
  waarbij alle invoer en uitvoer wordt onderschept. Het kijkt naar de
  escape-sequenties die worden verzonden en houdt een virtueel <q>venster</q>
  bij met wat het denkt dat op het scherm staat. Het maakt dus geen gebruik van
  kenmerken die specifiek zijn voor Linux en kan zonder al te veel moeite
  worden overgezet naar andere UNIX-achtige besturingssystemen.
</p>
</a11y-pkg>


<h2><a id="gui" name="gui">Grafische gebruikersinterfaces</a></h2>
<p>
Toegankelijkheid op het gebied van grafische gebruikersinterfaces op
UNIX-platformen heeft pas onlangs een belangrijke impuls gekregen door de
verschillende ontwikkelingsinspanningen rond de
<a href="http://www.gnome.org/">GNOME grafische werkomgeving</a>, met name het
<a href="https://wiki.gnome.org/Accessibility">GNOME toegankelijkheidsproject</a>.
</p>


<h2><a id="gnome" name="gnome">GNOME toegankelijkheidssoftware</a></h2>

<p>
  Een uitgebreide lijst is beschikbaar op de
  <a href="https://blends.debian.org/accessibility/tasks/gnome">taakpagina over
  Toegankelijk Gnome</a>
</p>

<a11y-pkg name="Assistive Technology Service Provider Interface" tag="at-spi">
<p>
  Dit pakket bevat de kernonderdelen van Toegankelijk Gnome.
  Het stelt aanbieders van ondersteunende technologie, zoals schermlezers, in
  staat om alle toepassingen die in de grafische omgeving actief zijn te
  doorzoeken op toegankelijkheidsgerelateerde informatie, en biedt
  overbruggingsmechanismen om andere gereedschappen dan GTK te ondersteunen.
</p>
<p>
  Koppelingen met de Python-taal worden geleverd in het pakket
  <a href="https://packages.debian.org/python-at-spi">python-at-spi</a>.
</p>
</a11y-pkg>
<a11y-pkg name="ATK accessibility toolkit" tag="atk">
<p>
  ATK is gereedschap dat toegankelijkheidsinterfaces biedt voor toepassingen of
  andere gereedschappen. Door deze interfaces te implementeren, kunnen die
  andere gereedschappen of toepassingen worden gebruikt met hulpmiddelen zoals
  schermlezers, vergrootglazen en andere alternatieve invoerapparatuur.
</p>
<p>
  Het runtime-gedeelte van ATK, dat nodig is om toepassingen te gebruiken die
  ermee gebouwd werden, is te vinden in het pakket
  <a href="https://packages.debian.org/libatk1.0-0">libatk1.0-0</a>.
  Ontwikkelingsbestanden voor ATK, nodig voor de compilatie van programma's of
  gereedschappen die het gebruiken, worden geleverd door het pakket
  <a href="https://packages.debian.org/libatk1.0-dev">libatk1.0-dev</a>.
  Koppelingen met de Ruby-taal worden geleverd in het pakket
  <a href="https://packages.debian.org/ruby-atk">ruby-atk</a>.
</p>
</a11y-pkg>
<a11y-pkg name="gnome-accessibility-themes" tag="gnome-accessibility-themes">
<p>
  Het pakket gnome-accessibility-themes bevat enkele zeer toegankelijke thema's
  voor de GNOME-desktopomgeving, ontworpen voor slechtzienden.
</p>
<p>
  Er zijn in totaal 7 thema's, met combinaties van hoog, laag of omgekeerd
  contrast en vergrote tekst en pictogrammen.
</p>
</a11y-pkg>
<a11y-pkg name="gnome-orca" tag="gnome-orca"
          url="http://live.gnome.org/Orca">
<p>
  Orca is een flexibele en uitbreidbare schermlezer die toegang biedt tot de
  grafische werkomgeving via door de gebruiker aanpasbare combinaties van
  spraak, braille en/of vergroting. Orca wordt sinds 2004 ontwikkeld door het
  Accessibility Program Office van Sun Microsystems, Inc. en is tot stand
  gekomen met vroege input van en voortdurende inschakeling van
  eindgebruikers.
</p>
<p>
  Orca kan <a href="#speech-dispatcher">Speech Dispatcher</a> gebruiken om
  spraakuitvoer te leveren aan de gebruiker. <a href="#brltty">BRLTTY</a> wordt
  gebruikt voor ondersteuning van brailleleesregels (en voor naadloze
  integratie van console- en GUI-brailleleesregels).
</p>
</a11y-pkg>


<h2><a id="kde" name="kde">KDE toegankelijkheidssoftware</a></h2>

<p>
  Een uitgebreide lijst is beschikbaar op de
  <a href="https://blends.debian.org/accessibility/tasks/kde">taakpagina over
  toegankelijkheid in KDE</a>
</p>

<a11y-pkg name="kmag" tag="kmag">
<p>
  Een deel van het scherm vergroten, net zoals u een lens zou gebruiken om de
  kleine lettertjes in een krant te lezen of een foto te vergroten. Deze
  toepassing is nuttig voor verschillende mensen: van onderzoekers tot
  kunstenaars, web-ontwerpers en mensen met slechtziendheid.
</p>
</a11y-pkg>


<h2><a id="input" name="input">Niet-standaard invoermethoden</a></h2>

<p>
  Een uitgebreide lijst is beschikbaar op de
  <a href="https://blends.debian.org/accessibility/tasks/input">taakpagina over
  invoermethodes</a>
</p>

<a11y-pkg name="Dasher" tag="dasher" url="http://www.inference.phy.cam.ac.uk/dasher/">
<p>
  Dasher is een informatie-efficiënte tekstinvoerinterface, aangedreven door
  natuurlijke continue aanwijsbewegingen. Dasher is een concurrentieel
  tekstinvoersysteem wanneer een toetsenbord van volledige grootte niet
  kan worden gebruikt - bijvoorbeeld
</p>
  <ul>
   <li>op een palmtopcomputer</li>
   <li>op draagbare accessoires</li>
   <li>bij het bedienen van een computer met één hand, met een joystick,
       touchscreen, trackball of muis</li>
   <li>bij het bedienen van een computer zonder handen (d.w.z. met hoofd-muis
       of met oogbesturing).</li>
  </ul>
<p>
  Met de oogbesturingsversie van Dasher kan een ervaren gebruiker even snel
  tekst schrijven als met normaal handschrift - 25 woorden per minuut; met een
  muis kunnen ervaren gebruikers 39 woorden per minuut schrijven.
</p>
<p>
  Dasher gebruikt een meer geavanceerd voorspellingsalgoritme dan het
  T9(tm)-systeem dat vaak in mobiele telefoons wordt gebruikt, waardoor het
  gevoelig is voor de omringende context.
</p>
</a11y-pkg>
<a11y-pkg name="Caribou" tag="caribou" url="https://wiki.gnome.org/Projects/Caribou">
<p>
  Caribou is een invoerondersteunende technologie bedoeld voor gebruikers van
  schakelaars en aanwijzers. Het biedt een configureerbaar schermtoetsenbord
  met scanmodus.
</p>
</a11y-pkg>

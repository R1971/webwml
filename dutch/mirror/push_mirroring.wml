#use wml::debian::template title="Push-spiegelen"
#use wml::debian::translation-check translation="82968214595db755ef0c09f5d6c1b11a6660b5cd"

<p>Push-spiegelen (push mirroring) is een vorm van spiegelen die de tijd
minimaliseert die nodig is voordat wijzigingen in het archief de spiegelservers
bereiken. De hoofdserver gebruikt een triggermechanisme om de
client-spiegelserver onmiddellijk te informeren dat deze moet worden
bijgewerkt.</p>

<p>Push-spiegelen kost meer moeite om op te zetten omdat de beheerders van de
bovenstroomse en de benedenstroomse spiegelserver informatie moeten uitwisselen.
Het voordeel is dat de bovenstroomse spiegelserver het spiegelingsproces
onmiddellijk start nadat zijn archief is bijgewerkt. Hierdoor kunnen wijzigingen
in het archief zich snel verspreiden.
</p>

<h2>Toelichting bij de methode</h2>

<p>Het triggeren gebeurt met behulp van ssh. De pushende server meldt zich via
ssh aan bij het spiegelserveraccount van de gepushte server met behulp van
authenticatie via een publieke sleutel. De sleutel is zo ingesteld dat deze
actie alleen een <q>mirror run</q> kan activeren, geen andere commando's. De
gepushte server voert dan zoals gebruikelijk ftpsync uit om het archief bij te
werken met rsync.
<br />
Het uitwisselen van publieke sleutels en het verlenen van een mogelijke toegang
tot begrensde rsync-servers vereist een coördinatie tussen de operator van een
spiegelserver en diens bovenstroomse bron.
</p>

<h2>Een spiegelserver opzetten als push-client</h2>

<p>Om een push-client te worden voor het FTP-archief, moet u spiegelen
instellen met onze standaard <a href="ftpmirror#how">ftpsync</a> scriptset.
<br />
Zodra dat werkt, voegt u de publieke ssh-sleutel van uw bovenstroomse
spiegelserver toe aan uw <code>~&lt;gebruiker&gt;/.ssh/authorized_keys</code>
met de restrictie <code>command="~/bin/ftpsync</code>. (Mogelijk heeft u ftpsync
in een andere map staan; pas dit dan daaraan aan.)
</p>

<h2>Sites die als primaire push-clients fungeren</h2>

<p>Primaire push-client-spiegelservers, ook wel Tier-1 spiegelservers genoemd,
zijn de push-client-spiegelservers die rechtstreeks synchroniseren vanaf het
interne syncproxy-netwerk van Debian.
</p>

<p>Als uw site <strong>zeer</strong> goed verbonden is (zowel een zeer goede
bandbreedte als goed verbonden met de belangrijkste backbones) en u bent bereid
om andere sites te laten spiegelen vanaf uw site, dan kunt u ons dat laten weten
zodat we u als push-spiegelserver in overweging kunnen nemen. Neem contact op
met het spiegelserverteam van Debian voor meer informatie over de opzet. Merk
echter op dat we niet alle verzoeken om een primaire push-spiegelserver te
worden kunnen accepteren, omdat we al een behoorlijk aantal Tier-1
spiegelservers hebben.
</p>

<h2>Een push-spiegelserver opzetten</h2>

<p>Gezien het grote aantal spiegelservers en de omvang van het Debian-archief,
is het niet haalbaar om alle spiegelservers de interne syncproxy's van Debian te
laten gebruiken als bovenstroomse bron voor Debian. Het is veel efficiënter als
de belasting wordt verdeeld over een aantal push-spiegelservers die over de hele
wereld verspreid zijn.
</p>

<p>Daarom zijn een aantal primaire push-clientsites op hun beurt push-servers
voor hun benedenstroomse clients. Als u uw site wilt configureren als een
push-server voor uw benedenstroomse sites, raadpleeg dan de
<a href="push_server">informatie over het instellen van een push-server</a>.
</p>
